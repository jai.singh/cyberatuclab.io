# cyberatuc.org
Current Cyber@UC website, built using Jekyll

## Installing dependencies
1. Install docker, docker.io package in apt for debian based.
2. Install docker-compose:

    pip install docker-compose

## Building and testing
1. Clone the repo:

    git clone git@gitlab.com:cyberatuc/cyberatuc.org

2. Run the development server:

    docker-compose up

3. (Optional) Run the builder via docker-compose for debugging:

    docker compuse run --rm cyberatuc.org bundle exec jekyll build -d public

4. Commit and push to gitlab, CI will update the live site in about 10 minutes


## Contributing
You can find information about how to edit this site at [cyberatuc.org/guides/website](https://www.cyberatuc.org/guides/website).
