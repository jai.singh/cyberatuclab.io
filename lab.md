---
layout: page
title: Lab
permalink: /lab/
---
Cyber@UC manages a lab space in ERC 516 (as well as a server room in ERC 513). All are welcome to come to our weekly lab sessions every Monday from 4:30 to 8pm and work on building up the lab. We are currently working on setting up the operating systems and software on the servers, configuring the network in both of our rooms, crimping ethernet cables, and various other tasks. Whether you're a sysadmin god or a complete newcomer, all are invited and encouraged to come and get some hands-on experience with enterprise-grade equipment!

## Main lab space (ERC 516)
{% include image.html src="2018-08-27-lab-entrance.jpg" type="left" alt="The front door to our lab room, with two students working inside" %}
Our main lab space is equipped with plenty of desks and computers for working and studying. Besides our weekly lab time, the lab is frequently open throughout the week, so feel free to stop by any time to do work or hang out.

## Server room (ERC 513)
{% include image.html src="2018-08-27-lab-racks-chris.jpg" type="left" alt="Messy room with server racks, with Chris hiding behind one of them" %}
Thanks to a grant from the state of Ohio, we have received $200k worth of server equipment to use for malware research, red/blue team simulations, and more. You can read more about how we're using this equipment in [this blog post]({{ site.url }}/blog/2018/08/31/security-operations-center-plans).
