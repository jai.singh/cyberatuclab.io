---
layout: page
title: App installation scripts for Ubuntu
permalink: /guides/app-installers
---
Here are some neato scripts for easily installing apps on Ubuntu. The apps will be installed into a reasonably-named folder in `/opt`, and a well-formed .desktop file will be added to `/usr/share/applications` so that the app appears in the OS application launcher.

## Android Studio

1. Download the latest version of Android Studio as a .tar.gz file from [developer.android.com/studio](https://developer.android.com/studio).
1. Download the script [android-studio.sh]({{ site.url }}/files/app-installers/android-studio.sh).
1. Run the script with `bash android-studio.sh android-studio-ide-linux.tar.gz` (replace `android-studio-ide-linux.tar.gz` with the name of your Android Studio .tar.gz file).

After it installs Android Studio, this particular script also fixes a permissions issue with running Android emulators.

## Ghidra

1. Download the latest version of Ghidra as a .zip file from [ghidra-sre.org](https://ghidra-sre.org/).
1. Download the script [ghidra.sh]({{ site.url }}/files/app-installers/ghidra.sh).
1. Run the script with `bash ghidra.sh ghidra_PUBLIC.zip` (replace `ghidra_PUBLIC.zip` with the name of your Ghidra .zip file).
