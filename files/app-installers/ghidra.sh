#!/usr/bin/env bash
# Usage: ./ghidra.sh [ghidra_PUBLIC.zip]

# make sure we have root privs
sudo -v

# extract to /opt
sudo unzip $1 -d /opt

# clean dir name
sudo mv /opt/ghidra_*_PUBLIC /opt/ghidra

# create desktop entry config file
cat > /tmp/ghidra.desktop << EOF
[Desktop Entry]
Type=Application
Version=1.0
Name=Ghidra
Comment=Software reverse engineering tool suite
Path=/opt/ghidra
Exec=/opt/ghidra/ghidraRun
Icon=/opt/ghidra/ghidra.png
Terminal=false
Categories=Development;Debugger
EOF

# download app icon (this PNG was extracted from /opt/ghidra/support/ghidra.ico)
wget -P /opt/ghidra https://cyberatuc.org/files/app-installers/ghidra.png

# install desktop entry into system applications folder
sudo desktop-file-install --dir=/usr/share/applications /tmp/ghidra.desktop
