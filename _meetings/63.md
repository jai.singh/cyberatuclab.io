---
title: "Contributing to the website"
date: 2018-08-08 18:30:00
meeting_number: 63
youtube: https://www.youtube.com/watch?v=mCzJG-JG9ec
---
This week, Hayden showed how to make updates to the chapter website, cyberatuc.org.

Our normal meeting room was unavailable because the CEAS Library had reduced hours for the week, so we held the meeting to Baldwin 755. Unfortunately, we ended up having to run the projector at an 800x600 video resolution due to the equipment in the room being slightly older, so please forgive the cramped/low-quality presentation in the video!

## Weekly content
* [Drupal Symfony Flaw](https://thehackernews.com/2018/08/symfony-drupal-hack.html)
* [Facebook Fizz](https://thehackernews.com/2018/08/fizz-tls-ssl-library.html)
* [WhatsApp Vulnerabilities](https://thehackernews.com/2018/08/whatsapp-modify-chat-fake-news.html)
* [New Method of Hacking WPA/WPA2](https://thehackernews.com/2018/08/how-to-hack-wifi-password.html)

### Recommended reading
* <https://www.welivesecurity.com/2018/07/26/fake-banking-apps-google-play-leak-stolen-credit-card-data/>
* <https://krebsonsecurity.com/2018/08/reddit-breach-highlights-limits-of-sms-based-authentication/>
* <https://www.welivesecurity.com/2018/08/02/reddit-reveals-breach-staffs-2fa/>
* <https://thehackernews.com/2018/08/ccleaner-software-download.html>
* <https://thehackernews.com/2018/08/android-9-pie.html>
* <https://thehackernews.com/2018/08/snapchat-hack-source-code.html>
* <https://krebsonsecurity.com/2018/08/florida-man-arrested-in-sim-swap-conspiracy/>
* <https://krebsonsecurity.com/2018/08/credit-card-issuer-tcm-bank-leaked-applicant-data-for-16-months/>
* <https://thehackernews.com/2018/08/fin7-carbanak-cobalt-hackers.html>
* <https://thehackernews.com/2018/08/mikrotik-router-hacking.html>
* <https://www.welivesecurity.com/2018/08/07/apple-chip-wannacryptor-shutdowns/>
* <https://thehackernews.com/2018/08/tsmc-iphone-computer-virus.html>
* <https://thehackernews.com/2018/08/tsmc-wannacry-ransomware-attack.html>

## Contributing to the website
See the [Contributing to cyberatuc.org](https://www.cyberatuc.org/guides/website) guide for more information.
