---
title: "Lab Updates"
date: 2019-03-27 18:30:00
meeting_number: 89
---
This week, AJ and Ryan presented information on our lab infrastructure to the club.

This meeting was not recorded but feel free to checkout our other meetings on youtube.
