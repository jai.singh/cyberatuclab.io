---
title: "REVOLUTION@UC CTF Review"
date: 2019-03-06 18:30:00
meeting_number: 87
---
This week we went over a handful of the problems we hosted for the REVOLUTION@UC CTF.

This meeting was not recorded but feel free to checkout our other meetings on youtube.
