---
title: "Networking Part 2: Networking Analysis"
date: 2018-10-02 18:30:00
meeting_number: 71
youtube: https://www.youtube.com/watch?v=8WWwOQaGhXw
---
This week, we talked further about networking and threat analysis.

## Weekly Content
* [Facebook vulnerability](https://newsroom.fb.com/news/2018/09/security-update/)
* Project Zero Finds New Linux Kernel Exploit
    * <https://thehackernews.com/2018/09/linux-kernel-exploit.html>
* Recommended reading
    * <https://thehackernews.com/2018/09/apple-server-hack.html>
    * <https://krebsonsecurity.com/2018/09/secret-service-warns-of-surge-in-atm-wiretapping-attacks/>
    * <https://thehackernews.com/2018/10/ghostdns-botnet-router-hacking.html>
    * <https://securelist.com/roaming-mantis-part-3/88071/>
    * <https://www.welivesecurity.com/2018/09/28/whos-behind-ddos-attacks-uk-universities/>

## Wireshark demo
* [Download Wireshark](https://www.wireshark.org/)
* [Answer key](https://www.honeynet.org/files/Forensic%20Challenge%202010%20-%20Scan%201%20-%20Solution_final.pdf)
