---
title: "Vulnerabilities and Exploits"
date: 2019-10-02 18:30:00
meeting_number: 102
youtube: https://www.youtube.com/watch?v=3PkLUDDkov4
---
This week we explained the basics of what vulnerabilities are, how they are reported, and how exploits for vulnerabilities are packaged and stored for use later

