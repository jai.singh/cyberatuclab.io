---
title: "Cool GitHub Projects! (actually)"
date: 2018-07-11 18:30:00
meeting_number: 59
youtube: https://www.youtube.com/watch?v=ysVkenM4gT0
---
This week, we looked at cool GitHub projects we had starred. We had originally planned to do this last meeting but ultimately postponed it.

## Weekly Content
* [Trustwave sued over failure to detect malware](https://www.bleepingcomputer.com/news/security/security-firm-sued-for-failing-to-detect-malware-that-caused-a-2009-breach/)
* [iOS USB restricted mode bypass](https://thehackernews.com/2018/07/bypass-ios-usb-restricted-mode.html)
* [Hybridized malware, is your computer worth it?](https://thehackernews.com/2018/07/cryptocurrency-mining-ransomware.html)
* Recommended reading
  * <https://www.welivesecurity.com/2018/07/11/polar-flow-app-exposes-geolocation-data-soldiers-secret-agents/>
  * <https://www.welivesecurity.com/2018/07/02/principle-least-privilege-strategy/>
  * <https://thehackernews.com/2018/07/facebook-cambridge-analytica.html>
  * <https://thehackernews.com/2018/07/intel-spectre-vulnerability.html>
  * <https://thehackernews.com/2018/07/arch-linux-aur-malware.html>
  * <https://thehackernews.com/2018/07/gaza-palestin-hacker.html>
  * <https://krebsonsecurity.com/2018/07/exxonmobil-bungles-rewards-card-debut/>
  * <https://krebsonsecurity.com/2018/07/notorious-hijack-factory-shunned-from-web/>
  * <https://krebsonsecurity.com/2018/06/plant-your-flag-mark-your-territory/>
