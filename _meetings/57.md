---
title: "SDR Fun!"
date: 2018-06-20 18:30:00
meeting_number: 57
youtube: https://youtube.com/watch?v=QLLdJqdqLuA
---
This week, we looked at some SDR (Software Defined Radio) stuff.

## Weekly Content
* [OpenBSD Disabling Hyperthreading](https://thehackernews.com/2018/06/openbsd-hyper-threading.html)
* [MacOS Bug Shows Data on Encrypted Drives](https://thehackernews.com/2018/06/apple-macos-quicklook.html)
* [Mobile Providers Cut 3rd Party Location Deals](https://krebsonsecurity.com/2018/06/verizon-to-stop-sharing-customer-location-data-with-third-parties/)
* [Alphabet Launches VirusTotal Monitor](http://blog.virustotal.com/2018/06/vtmonitor-to-mitigate-false-positives.html)
  * [DARKReading article](https://www.darkreading.com/operations/alphabet-launches-virustotal-monitor-to-stop-false-positives/d/d-id/1332104)
* [HeroRat](https://www.welivesecurity.com/2018/06/18/new-telegram-abusing-android-rat/)
* Additional articles
  * <https://www.darkreading.com/cloud/crowdstrike-secures-$200m-funding-round/d/d-id/1332088>
  * <https://www.crowdstrike.com/resources/news/crowdstrike-announces-200-million-series-e-financing-round/>
  * <https://www.crowdstrike.com/blog/crowdstrike-closes-200-million-series-e-financing-round-with-new-and-existing-investors/>
  * <https://krebsonsecurity.com/2018/06/bad-men-at-work-please-dont-click/>
  * <https://krebsonsecurity.com/2018/06/google-to-fix-location-data-leak-in-google-home-chromecast/>
  * <https://www.welivesecurity.com/2018/06/14/chile-revolutionize-cybersecurity-cyberattack/>
