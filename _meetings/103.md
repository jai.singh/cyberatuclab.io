---
title: "Reverse Engineering Basics"
date: 2019-10-23 18:30:00
meeting_number: 103
youtube: https://www.youtube.com/watch?v=wDbDHdcaHis
---
This week we explained the basics of Software Reverse Engineering, how you do it, and what it entails
